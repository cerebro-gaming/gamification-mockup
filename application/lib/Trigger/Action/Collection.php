<?php

namespace Lib\Trigger\Action;

use Cerebro\Api\Models\TriggersActions;

/**
 * Class Collection
 *
 * @package Lib\Trigger\Actions
 */
class Collection
{
    /**
     * @return array
     */
    public function getAll(): array
    {
        $events = TriggersActions::find();

        $out = [];
        foreach ($events as $k => $v) {
            $out[] = new Action($v->name, $v->id);
        }

        return $out;
    }
}