<?php

namespace Lib\Trigger\Action;

/**
 * Class Action
 *
 * @package Lib\Trigger\Actions
 */
class Action implements \JsonSerializable
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $id;

    /**
     * Action constructor.
     *
     * @param string $name
     * @param int    $id
     */
    public function __construct(string $name, int $id)
    {
        $this->name = $name;
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }


}