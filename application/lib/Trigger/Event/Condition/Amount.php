<?php

namespace Lib\Trigger\Event\Condition;

/**
 * Class Amount
 *
 * @package Lib\Trigger\Event\Condition
 */
class Amount extends AbstractCondition
{
    /**
     * Amount constructor.
     *
     * @param int    $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name);
        $this->signs = [
            self::SIGN_LTE,
            self::SIGN_LT,
            self::SIGN_GTE,
            self::SIGN_GT,
            self::SIGN_NE,
            self::SIGN_EQ
        ];
    }
}