<?php

namespace Lib\Trigger\Event\Condition;

/**
 * Class LastName
 *
 * @package Lib\Trigger\Event\Condition
 */
class LastName extends AbstractCondition
{
    /**
     * LastName constructor.
     *
     * @param int    $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name);
        $this->signs = [
            self::SIGN_NE,
            self::SIGN_EQ
        ];
    }
}