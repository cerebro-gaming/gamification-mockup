<?php

namespace Lib\Trigger\Event\Condition;

use Lib\Trigger\TriggersException;

/**
 * Class ConditionFactory
 *
 * @package Lib\Trigger\Event\Condition
 */
class ConditionFactory
{
    const TYPE_DATE = 'Date';
    const TYPE_COUNTRY = 'Country';
    const TYPE_DOB = 'DOB';
    const TYPE_FNAME = 'FirstName';
    const TYPE_LNAME = 'LastName';
    const TYPE_AMOUNT = 'Amount';
    const TYPE_VARIABLE = 'Variable';
    const TYPE_VARIABLE_VALUE = 'VariableValue';
    const TYPE_VARIABLE_VALUE_DIFF = 'VariableValueDiff';

    /**
     * @param int    $id
     * @param string $type
     *
     * @return \Lib\Trigger\Event\Condition\AbstractCondition
     * @throws \Lib\Trigger\TriggersException
     */
    public static function create(int $id, string $type): AbstractCondition
    {
        $type = 'self::TYPE_' . $type;
        if (!defined($type)) {
            throw new TriggersException('Unknown condition type ' . $type);
        }

        $className = '\Lib\Trigger\Event\Condition\\' . constant($type);
        $object = new $className($id, constant($type));

        return $object;

    }
}