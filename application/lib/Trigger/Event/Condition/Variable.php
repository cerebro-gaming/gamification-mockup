<?php

namespace Lib\Trigger\Event\Condition;

use Cerebro\Api\Models\TriggersVariables;

class Variable extends AbstractCondition
{
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name);

        $this->signs = [
            self::SIGN_NE,
            self::SIGN_EQ
        ];

        $variables = TriggersVariables::find();
        $this->values = [];
        foreach($variables as $v) {
            $this->values[] = $v->name;
        }
    }
}