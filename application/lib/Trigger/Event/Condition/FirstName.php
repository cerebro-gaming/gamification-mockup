<?php

namespace Lib\Trigger\Event\Condition;

/**
 * Class FirstName
 *
 * @package Lib\Trigger\Event\Condition
 */
class FirstName extends AbstractCondition
{
    /**
     * FirstName constructor.
     *
     * @param int    $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name);

        $this->signs = [
            self::SIGN_NE,
            self::SIGN_EQ
        ];
    }
}