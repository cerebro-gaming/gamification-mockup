<?php

namespace Lib\Trigger\Event\Condition;

use Lib\Trigger\TriggersException;

/**
 * Class AbstractCondition
 *
 * @package Lib\Trigger\Event\Condition
 */
abstract class AbstractCondition implements \JsonSerializable
{
    const CONDITION_AND = 'and';
    const CONDITION_OR = 'or';
    const CONDITION_NOT = 'not';

    const SIGN_NE = 'ne';
    const SIGN_EQ = 'eq';
    const SIGN_GT = 'gt';
    const SIGN_LT = 'lt';
    const SIGN_GTE = 'gte';
    const SIGN_LTE = 'lte';

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $condition;

    /**
     * @var string
     */
    public $result;

    /**
     * @var
     */
    public $attribute;

    /**
     * @var array
     */
    public $signs;

    /**
     * @var array
     */
    public $values;

    /**
     * @var \Lib\Trigger\Event\Condition\AbstractCondition[]
     */
    public $chain = [];

    /**
     * AbstractCondition constructor.
     *
     * @param int    $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            //'condition' => $this->condition,
            'signs' => $this->signs,
            'values' => $this->values
        ];
    }

    /**
     * @param string $condition
     *
     * @throws \Lib\Trigger\TriggersException
     */
    public function setCondition(string $condition)
    {
        if (!in_array($condition, [
            self::CONDITION_AND,
            self::CONDITION_OR,
            self::CONDITION_NOT
        ])) {
            throw new TriggersException('Condition is invalid');
        }

        $this->condition = $condition;
    }

    /**
     * @param array $values
     */
    public function setValues(array $values)
    {
        $this->values = $values;
    }

    /**
     * @param mixed $element
     */
    public function addValue($element)
    {
        $this->values[] = $element;
    }

    /**
     * @param \Lib\Trigger\Event\Condition\AbstractCondition $chain
     */
    public function addChain(AbstractCondition $chain)
    {
        $this->chain[] = $chain;
    }
}