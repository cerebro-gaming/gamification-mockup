<?php

namespace Lib\Trigger\Event;

/**
 * Class Collection
 *
 * @package Lib\Trigger\Events
 */
class Collection
{
    /**
     * @return array
     */
    public function getAll(): array
    {
        $events = \Cerebro\Api\Models\TriggersEvents::find();

        $out = [];
        foreach ($events as $k => $v) {
            $out[] = new Event($v->name, $v->id);
        }

        return $out;
    }
}