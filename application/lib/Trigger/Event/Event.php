<?php

namespace Lib\Trigger\Event;

/**
 * Class Event
 *
 * @package Lib\Trigger\Events
 */
class Event implements \JsonSerializable
{
    protected $name;
    protected $id;

    /**
     * Event constructor.
     *
     * @param string $name
     * @param int    $id
     */
    public function __construct(string $name, int $id)
    {
        $this->name = $name;
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }


}