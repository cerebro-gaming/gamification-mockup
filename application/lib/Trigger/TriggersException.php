<?php

namespace Lib\Trigger;

/**
 * Class TriggersException
 *
 * @package Lib\Trigger
 */
class TriggersException extends \Exception
{
    /**
     * Exception constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, \Throwable $previous = null)
    {
        $message = get_called_class() . ': ' . $message;
        parent::__construct($message, $code, $previous);
    }

}