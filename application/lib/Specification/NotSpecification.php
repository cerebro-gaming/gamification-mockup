<?php

namespace Lib\Specification;

/**
 * Class NotSpecification
 *
 * @package Lib\Specification
 */
class NotSpecification implements SpecificationInterface
{
    /**
     * @var SpecificationInterface
     */
    private $specification;

    /**
     * NotSpecification constructor.
     *
     * @param \Lib\Specification\SpecificationInterface $specification
     */
    public function __construct(SpecificationInterface $specification)
    {
        $this->specification = $specification;
    }

    /**
     * @param \Lib\Specification\SpecificationElement $item
     *
     * @return bool
     */
    public function isSatisfied(SpecificationElement $item): bool
    {
        return !$this->specification->isSatisfied($item);
    }
}