<?php

namespace Lib\Specification;

/**
 * Class OrSpecification
 *
 * @package Lib\Specification
 */
class OrSpecification implements SpecificationInterface
{
    /**
     * @var SpecificationInterface[]
     */
    private $specifications;

    /**
     * @param SpecificationInterface[] ...$specifications
     */
    public function __construct(SpecificationInterface ...$specifications)
    {
        $this->specifications = $specifications;
    }

    /**
     * @param \Lib\Specification\SpecificationElement $item
     *
     * @return bool
     */
    public function isSatisfied(SpecificationElement $item): bool
    {
        foreach ($this->specifications as $specification) {
            if ($specification->isSatisfied($item)) {
                return true;
            }
        }
        return false;
    }
}