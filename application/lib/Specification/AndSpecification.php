<?php

namespace Lib\Specification;

/**
 * Class AndSpecification
 *
 * @package Lib\Specification
 */
class AndSpecification implements SpecificationInterface
{
    /**
     * @var SpecificationInterface[]
     */
    private $specifications;

    /**
     * @param SpecificationInterface[] ...$specifications
     */
    public function __construct(SpecificationInterface ...$specifications)
    {
        $this->specifications = $specifications;
    }

    /**
     * @param \Lib\Specification\SpecificationElement $item
     *
     * @return bool
     */
    public function isSatisfied(SpecificationElement $item): bool
    {
        foreach ($this->specifications as $specification) {
            if (!$specification->isSatisfied($item)) {
                return false;
            }
        }

        return true;
    }
}