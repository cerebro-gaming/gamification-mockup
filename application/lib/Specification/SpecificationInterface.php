<?php

namespace Lib\Specification;

/**
 * Interface SpecificationInterface
 *
 * @package Lib\Specification
 */
interface SpecificationInterface
{
    /**
     * @param \Lib\Specification\SpecificationElement $element
     *
     * @return bool
     */
    public function isSatisfied(SpecificationElement $element): bool;
}