<?php

namespace Lib\Core;

/**
 * Class ModelException
 *
 * @package Lib\Core
 */
class CoreException extends \Exception
{
    /**
     * CoreException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        $message = get_called_class() . ': ' . $message;
        parent::__construct($message, $code, $previous);
    }
}
