<?php

namespace Lib\Core\Service;

use Phalcon\Di\FactoryDefault;

/**
 * Class Url
 *
 * @package Lib\Core\Service
 */
class Url
{
    /**
     * @param FactoryDefault $di
     */
    public static function factory(FactoryDefault $di)
    {
        /**
         * The URL component is used to generate all kinds of URLs in the application
         */
        $di->setShared('url', function () use ($di) {
            $url = new \Phalcon\Mvc\Url();
            $config = $di->get('config');
            $url->setBaseUri($config->application->baseUri);

            return $url;
        });
    }
}
