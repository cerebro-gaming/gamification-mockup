<?php

namespace Lib\Core\Service;

use Phalcon\Cache\Backend\Redis as BackendCache;
use Phalcon\Cache\Frontend\Data as FrontCache;
use Phalcon\Di\FactoryDefault;

/**
 * Class Cache
 *
 * @package Lib\Core\Service
 */
class Cache
{
    /**
     * @param FactoryDefault $di
     */
    public static function factory(FactoryDefault $di)
    {
        /**
         * Cache system
         */
        $di->setShared('systemCache', function (int $lifetime = 3600) use ($di) {
            $config = $di->get('config');

            // front cache
            $frontCache = new FrontCache([
                'lifetime' => $lifetime
            ]);

            // backend cache
            $cache = new BackendCache($frontCache, [
                'host' => $config->redis->host,
                'port' => $config->redis->port,
                'persistent' => $config->redis->persistent
            ]);

            return $cache;
        });

        /**
         * Core Cache
         */
        $di->setShared('cache', function () use ($di) {
            return new \Bsc\Cache\Cache();
        });
    }
}
