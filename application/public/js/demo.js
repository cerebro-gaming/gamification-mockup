type = ['', 'info', 'success', 'warning', 'danger'];


demo = {
    initPickColor: function () {
        $('.pick-class-label').click(function () {
            var new_class = $(this).attr('new-class');
            var old_class = $('#display-buttons').attr('data-class');
            var display_div = $('#display-buttons');
            if (display_div.length) {
                var display_buttons = display_div.find('.btn');
                display_buttons.removeClass(old_class);
                display_buttons.addClass(new_class);
                display_div.attr('data-class', new_class);
            }
        });
    },

    initChartist: function () {

        var dataSales = {
            labels: ['9:00AM', '12:00AM', '3:00PM', '6:00PM', '9:00PM', '12:00PM', '3:00AM', '6:00AM'],
            series: [
                [287, 385, 490, 562, 594, 626, 698, 895, 952],
                [67, 152, 193, 240, 387, 435, 535, 642, 744],
                [23, 113, 67, 108, 190, 239, 307, 410, 410]
            ]
        };

        var optionsSales = {
            lineSmooth: false,
            low: 0,
            high: 1000,
            showArea: true,
            height: "245px",
            axisX: {
                showGrid: false,
            },
            lineSmooth: Chartist.Interpolation.simple({
                divisor: 3
            }),
            showLine: true,
            showPoint: false,
        };

        var responsiveSales = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Line('#chartHours', dataSales, optionsSales, responsiveSales);


        var data = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [
                [542, 543, 520, 680, 653, 753, 326, 434, 568, 610, 756, 895],
                [230, 293, 380, 480, 503, 553, 600, 664, 698, 710, 736, 795]
            ]
        };

        var options = {
            seriesBarDistance: 10,
            axisX: {
                showGrid: false
            },
            height: "245px"
        };

        var responsiveOptions = [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 5,
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Line('#chartActivity', data, options, responsiveOptions);

        var dataPreferences = {
            series: [
                [25, 30, 20, 25]
            ]
        };

        var optionsPreferences = {
            donut: true,
            donutWidth: 40,
            startAngle: 0,
            total: 100,
            showLabel: false,
            axisX: {
                showGrid: false
            }
        };

        Chartist.Pie('#chartPreferences', dataPreferences, optionsPreferences);

        Chartist.Pie('#chartPreferences', {
            labels: ['62%', '32%', '6%'],
            series: [62, 32, 6]
        });
    },

    initGoogleMaps: function () {
        var myLatlng = new google.maps.LatLng(40.748817, -73.985428);
        var mapOptions = {
            zoom: 13,
            center: myLatlng,
            scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
            styles: [{
                "featureType": "water",
                "stylers": [{"saturation": 43}, {"lightness": -11}, {"hue": "#0088ff"}]
            }, {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [{"hue": "#ff0000"}, {"saturation": -100}, {"lightness": 99}]
            }, {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{"color": "#808080"}, {"lightness": 54}]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ece2d9"}]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ccdca1"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#767676"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [{"color": "#ffffff"}]
            }, {"featureType": "poi", "stylers": [{"visibility": "off"}]}, {
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#b8cb93"}]
            }, {"featureType": "poi.park", "stylers": [{"visibility": "on"}]}, {
                "featureType": "poi.sports_complex",
                "stylers": [{"visibility": "on"}]
            }, {"featureType": "poi.medical", "stylers": [{"visibility": "on"}]}, {
                "featureType": "poi.business",
                "stylers": [{"visibility": "simplified"}]
            }]

        }
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            title: "Hello World!"
        });

        // To add the marker to the map, call setMap();
        marker.setMap(map);
    },

    showNotification: function (from, align) {
        color = Math.floor((Math.random() * 4) + 1);

        $.notify({
            icon: "ti-gift",
            message: "Welcome to <b>Paper Dashboard</b> - a beautiful freebie for every web developer."

        }, {
            type: type[color],
            timer: 4000,
            placement: {
                from: from,
                align: align
            }
        });
    },
    initDocumentationCharts: function () {
        //     	init single simple line chart
        var dataPerformance = {
            labels: ['6pm', '9pm', '11pm', '2am', '4am', '8am', '2pm', '5pm', '8pm', '11pm', '4am'],
            series: [
                [1, 6, 8, 7, 4, 7, 8, 12, 16, 17, 14, 13]
            ]
        };

        var optionsPerformance = {
            showPoint: false,
            lineSmooth: true,
            height: "200px",
            axisX: {
                showGrid: false,
                showLabel: true
            },
            axisY: {
                offset: 40,
            },
            low: 0,
            high: 16,
            height: "250px"
        };

        Chartist.Line('#chartPerformance', dataPerformance, optionsPerformance);

        //     init single line with points chart
        var dataStock = {
            labels: ['\'07', '\'08', '\'09', '\'10', '\'11', '\'12', '\'13', '\'14', '\'15'],
            series: [
                [22.20, 34.90, 42.28, 51.93, 62.21, 80.23, 62.21, 82.12, 102.50, 107.23]
            ]
        };

        var optionsStock = {
            lineSmooth: false,
            height: "200px",
            axisY: {
                offset: 40,
                labelInterpolationFnc: function (value) {
                    return '$' + value;
                }

            },
            low: 10,
            height: "250px",
            high: 110,
            classNames: {
                point: 'ct-point ct-green',
                line: 'ct-line ct-green'
            }
        };

        Chartist.Line('#chartStock', dataStock, optionsStock);

        //      init multiple lines chart
        var dataSales = {
            labels: ['9:00AM', '12:00AM', '3:00PM', '6:00PM', '9:00PM', '12:00PM', '3:00AM', '6:00AM'],
            series: [
                [287, 385, 490, 562, 594, 626, 698, 895, 952],
                [67, 152, 193, 240, 387, 435, 535, 642, 744],
                [23, 113, 67, 108, 190, 239, 307, 410, 410]
            ]
        };

        var optionsSales = {
            lineSmooth: false,
            low: 0,
            high: 1000,
            showArea: true,
            height: "245px",
            axisX: {
                showGrid: false,
            },
            lineSmooth: Chartist.Interpolation.simple({
                divisor: 3
            }),
            showLine: true,
            showPoint: false,
        };

        var responsiveSales = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Line('#chartHours', dataSales, optionsSales, responsiveSales);

        //      pie chart
        Chartist.Pie('#chartPreferences', {
            labels: ['62%', '32%', '6%'],
            series: [62, 32, 6]
        });

        //      bar chart
        var dataViews = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [
                [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]
            ]
        };

        var optionsViews = {
            seriesBarDistance: 10,
            classNames: {
                bar: 'ct-bar'
            },
            axisX: {
                showGrid: false,

            },
            height: "250px"

        };

        var responsiveOptionsViews = [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 5,
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Bar('#chartViews', dataViews, optionsViews, responsiveOptionsViews);

        //     multiple bars chart
        var data = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [
                [542, 543, 520, 680, 653, 753, 326, 434, 568, 610, 756, 895],
                [230, 293, 380, 480, 503, 553, 600, 664, 698, 710, 736, 795]
            ]
        };

        var options = {
            seriesBarDistance: 10,
            axisX: {
                showGrid: false
            },
            height: "245px"
        };

        var responsiveOptions = [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 5,
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Line('#chartActivity', data, options, responsiveOptions);

    }

};

triggers = {
    getEvents: function () {
        $.get('/api/v1/triggers/events', function (d) {
            var events = d.data;
            $.each(events, function (k, v) {
                var input = $('<option>' + v.name + '</option>');
                input.val(v.id);

                $('.g-events').append(input);
            });
        });

        $('.g-events').on('change', function () {
            $('.g-conditions').html(' ');
        });
    },

    getActions: function () {
        $.get('/api/v1/triggers/actions', function (d) {
            var actions = d.data;
            $.each(actions, function (k, v) {
                var input = $('<option>' + v.name + '</option>');
                input.val(v.id);

                $('.g-actions').append(input);
            });
        });
    },

    getConditions: function (eventId) {
        $.get('/api/v1/triggers/events/' + eventId + '/conditions', function (d) {
            var conditions = d.data;
            triggers.createConditionHtml(conditions);

            return conditions;
        });
    },

    createConditionHtml: function (conditions) {
        var c = $('<p class="g-p-condition"><i class="fa fa-minus-square" aria-hidden="true" style="cursor: pointer"></i> &nbsp; If &nbsp; \n' +
            ' <select name="condition-name" class="form-control mb-2 mb-sm-0 border-input g-conditions-select"><option>-- select --</option></select>\n' +
            ' <select name="condition-sign" class="form-control mb-2 mb-sm-0 border-input g-conditions-signs"><option>-- select --</option></select>\n' +
            ' </p>');

        var input = $('<input name="condition-value" type="text" class="form-control border-input g-conditions-value" placeholder="value"/>');
        var select = $('<select name="condition-value" class="form-control mb-2 mb-sm-0 border-input g-conditions-value"><option>-- select --</option></select>');

        $.each(conditions, function (k, v) {
            var opt = $('<option/>');
            $(opt).text(v.name);
            $(opt).data('signs', JSON.stringify(v.signs));
            $(opt).data('values', JSON.stringify(v.values));
            $(opt).val(v.id);

            $(c).find('.g-conditions-select').append(opt);
        });

        $(c).find('.g-conditions-select').on('change', function () {
            var signs = $(c).find('.g-conditions-select').find('option:selected').data('signs').toString();
            var values = $(c).find('.g-conditions-select').find('option:selected').data('values').toString();
            signs = JSON.parse(signs);
            values = JSON.parse(values);

            $(c).find('.g-conditions-signs').find('option').remove();
            $(signs).each(function (k, v) {
                var o = $('<option>' + v + '</option>');
                o.val(v);
                $(c).find('.g-conditions-signs').append(o);
            });

            // condition values
            $(c).find('.g-conditions-value').remove();
            if (values && (typeof values === 'object')) {
                $.each(values, function (k, v) {
                    var option = $('<option />');
                    option.val(k);
                    option.text(v);
                    $(select).append(option);
                });

                $(c).append(select);
            } else {
                $(c).append(input);
            }
        });

        $(c).find('.fa-minus-square').click(function () {
            $(this).closest('p').remove();
        });

        $('.g-conditions').append(c);
    },

    createFormJson: function () {
        var json = {
            name: null,
            event: {},
            action: {},
            action_details: [],
            condition: 'AND',
            conditions: []
        };

        // event
        $('select[name^=event]').each(function (k, v) {
            json.event = {
                id: parseInt($(this).val())
            }
        });

        if (isNaN(json.event.id)) {
            json.event.id = null;
        }

        // action
        $('select[name^=action\\\[id\\\]]').each(function (k, v) {
            json.action = {
                id: parseInt($(this).val())
            }
        });

        // conditions
        $('.g-p-condition').each(function (k, v) {
            var cond = {
                id: parseInt($(this).find('select[name=condition-name]').val()),
                sign: $(this).find('select[name=condition-sign]').val(),
                value: $(this).find('select[name=condition-value], input[name=condition-value]').val()
            };

            json.conditions.push(cond);
        });

        // action details
        $('.g-action-details-' + json.action.id).find('.form-inline').each(function(kk,vv){
            var details = {};
            $(vv).find('input, select, textarea').each(function (k, v) {
                var attr = $(this).attr('name');
                details[attr] = $(this).val();
            });

            json.action_details.push(details);
        });

        json.name = $('#trigger_name').val();

        return json;
    },

    save: function () {
        var json = triggers.createFormJson();
        var triggerId = $('#trigger_id').val();
        var url = '/backoffice/triggers/save';

        if (typeof triggerId !== 'undefined') {
            url += '/' + triggerId;
        }

        $.post(url, json, function (d) {
            console.log('RETURN');
            console.log(d);
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            if(typeof d.errors === 'undefined') {
                toastr["success"]("Success", "Trigger save");
            } else {
                var msg = '';
                for(var i in d.errors) {
                    msg += d.errors[i].status;
                }
                toastr["error"](msg, "Error");
            }
        })
    }
};

var triggerId = $('#trigger_id').val();
var url = '/backoffice/triggers/save';

if (typeof triggerId === 'undefined') {
    //triggers.getEvents();
    //triggers.getActions();
}


$('.g-add-condition').click(function (e) {
    e.preventDefault();
    var id = $(this).closest('.row').find('.g-events').val();
    triggers.getConditions(id);
    return true;
});

$('.g-add-property').click(function (e) {
    e.preventDefault();
    var last = $('.g-property').last();
    $(last).after(last.clone());
    return true;
});

$('.g-actions').on('change', function () {
    var v = parseInt($(this).val());
    $('.g-action-details').hide();
    $('.g-action-details-' + v).show();
});

$('.g-events').on('change', function() {
    $('.g-p-condition').remove();
});