<?php

/**
 * Register application modules
 */
$application->registerModules(array(
    'Api' => array(
        'className' => 'Cerebro\Api\Module',
        'path' => __DIR__ . '/../apps/api/Module.php'
    ),
    'Backoffice' => [
        'className' => 'Cerebro\Backoffice\Module',
        'path' => __DIR__ . '/../apps/backoffice/Module.php'
    ]
));
