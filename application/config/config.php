<?php

return new \Phalcon\Config([
    'database' => [
        'adapter'  => 'Mysql',
        'host'     => 'gamificationmockup_mysql_1',
        'username' => 'super',
        'password' => 'secret',
        'dbname'   => 'cerebro',
        'charset'  => 'utf8',
    ],
    'redis' => [
        'host' => 'localhost',
        'username' => '',
        'password' => '',
        'port' => 6379,
        'persistent' => true
    ]
]);
