<?php
/**
 * loader
 */

include_once __DIR__ . '/../vendor/autoload.php';

// Creates the autoloader
$loader = new \Phalcon\Loader();
$namespaces = $loader->getNamespaces();

// Register some namespaces
$loader->registerNamespaces([
    'Lib' => __DIR__ . '/../lib'
]);

// Register autoloader
$loader->register();
