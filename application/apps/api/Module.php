<?php

namespace Cerebro\Api;

use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\ModuleDefinitionInterface;

/**
 * Class Module
 *
 * @package Cerebro\Api
 */
final class Module implements ModuleDefinitionInterface
{
    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();

        $loader->registerNamespaces(array(
            'Cerebro\Api\Controllers' => __DIR__ . '/controllers/',
            'Cerebro\Api\Models' => __DIR__ . '/models/'
        ));
        $loader->register();
    }

    /**
     * Registers services related to the module
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {
        $config = include __DIR__ . '/config/config.php';
        $di->set('config', $di->get('config')->merge($config));

        // basic view - won't be used but must be registered
        $di->set('view', function () {
            return new \Phalcon\Mvc\View();
        }, true);
    }
}
