<?php
/**
 * @var \Phalcon\Mvc\Router $router
 */

$api = new \Phalcon\Mvc\Router\Group([
    'module' => 'Api',
    'namespace' => '\\Cerebro\\Api\\Controllers',
]);
$api->setPrefix('/api/v1');

$api->add('/triggers/events', ['controller' => 'triggers', 'action' => 'events'], ['GET']);
$api->add('/triggers/actions', ['controller' => 'triggers', 'action' => 'actions'], ['GET']);
$api->add('/triggers/events/:int/conditions', ['controller' => 'triggers', 'action' => 'conditions', 'eventId' => 1], ['GET']);
$api->add('/test', ['controller' => 'index', 'action' => 'test'], ['GET']);

$router->mount($api);