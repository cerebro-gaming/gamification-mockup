<?php

namespace Cerebro\Api\Models;

final class Games extends \Phalcon\Mvc\Model
{
    public $id;

    public $company;

    public $name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("cerebro");
        $this->setSource("games");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'games';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return \Cerebro\Api\Models\TriggersSaved[]|Games|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Games|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}
