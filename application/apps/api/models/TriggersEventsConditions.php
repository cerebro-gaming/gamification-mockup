<?php

namespace Cerebro\Api\Models;

use Lib\Helpers\Sort;
use Lib\Trigger\Event\Condition\ConditionFactory;
use Phalcon\Mvc\Model\ResultsetInterface;
use Phalcon\Text;

/**
 * Class TriggersEventsConditions
 */
final class TriggersEventsConditions extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $eventId;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $code;

    public function initialize()
    {
        $this->setSchema("cerebro");
        $this->setSource("triggers_events_conditions");
        $this->belongsTo('event_id', '\TriggersEvents', 'id', ['alias' => 'TriggersEvents']);
    }

    public function columnMap()
    {
        $columns = $this->getModelsMetaData()->getAttributes($this);
        $map = [];
        foreach ($columns as $column) {
            $map[$column] = lcfirst(Text::camelize($column));
        }

        return $map;
    }

    public function getSource()
    {
        return 'triggers_events_conditions';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function getByEventId(int $eventId): array
    {
        $data = self::find([
            "eventId = :id:",
            "bind" => [
                "id" => $eventId
            ]
        ]);
        $triggerConditions = [];
        foreach ($data as $k => $v) {
            $triggerConditions[] = ConditionFactory::create($v->id, $v->code);
        }

        Sort::reindexByKey($triggerConditions, 'id');

        return $triggerConditions;
    }

}
