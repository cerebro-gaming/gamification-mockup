<?php

namespace Cerebro\Api\Controllers;

/**
 * Class IndexController
 *
 * @package Cerebro\Api\Controllers
 */
final class IndexController extends ControllerBase
{
    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function indexAction()
    {
        $this->apiResponse->setData(true);
        return $this->sendApiResponse();
    }

    public function testAction()
    {
        $this->apiResponse->setData(['html' => '<style>.boxy {width:200px; height:200px; background:#2b542c; color:#ffffff; text-align: center; display: table;font-size: 30pt;}.boxy span {  vertical-align: middle;  display: table-cell;}                                .rotate-scale-down-hor{-webkit-animation:rotate-scale-down-hor .65s linear both;animation:rotate-scale-down-hor .65s linear both} @-webkit-keyframes rotate-scale-down-hor{0%{-webkit-transform:scale(1) rotateX(0);transform:scale(1) rotateX(0)}50%{-webkit-transform:scale(.5) rotateX(-180deg);transform:scale(.5) rotateX(-180deg)}100%{-webkit-transform:scale(1) rotateX(-360deg);transform:scale(1) rotateX(-360deg)}}@keyframes rotate-scale-down-hor{0%{-webkit-transform:scale(1) rotateX(0);transform:scale(1) rotateX(0)}50%{-webkit-transform:scale(.5) rotateX(-180deg);transform:scale(.5) rotateX(-180deg)}100%{-webkit-transform:scale(1) rotateX(-360deg);transform:scale(1) rotateX(-360deg)}}</style><div class="rotate-scale-down-hor boxy"><span>Cerebro!</span></div>']);
        return $this->sendApiResponse();
    }
}
