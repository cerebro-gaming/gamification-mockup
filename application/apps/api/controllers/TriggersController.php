<?php

namespace Cerebro\Api\Controllers;

use Cerebro\Api\Models\TriggersEventsConditions;
use Lib\Trigger\Event\Condition\ConditionFactory;

final class TriggersController extends ControllerBase
{
    /*
     * Returns list of events
     */
    public function eventsAction(): \Phalcon\Http\ResponseInterface
    {
        $events = new \Lib\Trigger\Event\Collection();
        $this->response->setJsonContent(['data' => $events->getAll()]);

        return $this->response;
    }

    /*
     * Actions
     */
    public function actionsAction(): \Phalcon\Http\ResponseInterface
    {
        $actions = new \Lib\Trigger\Action\Collection();
        $this->response->setJsonContent(['data' => $actions->getAll()]);

        return $this->response;
    }

    /*
     * Conditions
     */
    public function conditionsAction(int $eventId): \Phalcon\Http\ResponseInterface
    {
        $conditions = new TriggersEventsConditions();

        $triggersEventsConditions = $conditions->find([
            "eventId = :id:",
            "bind" => [
                "id" => $eventId
            ]
        ]);

        /*
         * conditions not found for that id
         */
        if (!$triggersEventsConditions->toArray()) {
            return $this->notFound();
        }

        /*
         * create output data
         */
        $data = [];
        foreach ($triggersEventsConditions as $k => $v) {
            $data[] = ConditionFactory::create($v->id, $v->code);
        }

        $this->apiResponse->setData($data);
        return $this->respond();
    }

    /*
     * Save
     */
    public function saveAction()
    {

        return $this->respond();
    }
}
