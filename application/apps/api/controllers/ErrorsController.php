<?php

namespace Cerebro\Api\Controllers;

use Lib\Api\Error;

/**
 * Class ErrorsController
 *
 * @link    http://jsonapi.org/format/#errors
 * @package Cerebro\Api\Controllers
 */
final class ErrorsController extends ControllerBase
{
    /**
     * 404 error page
     */
    public function notFoundAction()
    {
        $this->response->setStatusCode(404, 'Not Found');
        $this->view->disable();

        $error = new Error();
        $error->setTitle('Not Found');
        $error->setDetail('You should read our API docs before using it! :)');
        $this->apiResponse->setError($error);

        return $this->sendApiResponse();
    }

    /**
     * 403 Forbidden
     */
    public function forbiddenAction()
    {
        $this->response->setStatusCode(403, 'Forbidden');

        $error = new Error();
        $error->setTitle('Forbidden');
        $error->setDetail('You don\'t have access to this endpoint');
        $this->apiResponse->setError($error);

        return $this->sendApiResponse();
    }

    /**
     * 405 Method Not Allowed
     */
    public function methodNotAllowedAction()
    {
        $this->response->setStatusCode(405, 'Method Not Allowed');

        $error = new Error();
        $error->setTitle('Method Not Allowed');
        $error->setDetail('Method '.$this->request->getMethod().' is not allowed');
        $this->apiResponse->setError($error);

        return $this->sendApiResponse();
    }

    /**
     * 500 error page
     */
    public function exceptionAction()
    {
        $this->response->setStatusCode(500, 'Internal server error');
        $this->view->disable();

        /**
         * @var \Exception $e
         */
        $e = $this->dispatcher->getParam('exception');
        $error = new Error();
        if (!isProd()) {
            $error->setDetail($e->getTraceAsString());
        }
        $error->setTitle($e->getMessage());
        $this->apiResponse->setError($error);

        return $this->sendApiResponse();
    }
}
