<?php

namespace Cerebro\Backoffice\Controllers;

final class AuthController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->setLayout('auth');
    }

    public function logoutAction()
    {
        $this->session->destroy(true);

        return $this->response->redirect('/backoffice/login');
    }

    public function processAction()
    {
        $hash = '$2y$08$WllZU3UyT1VKNmFVWXM5QOnEE0lrg6Vyvi/V1uMpBFlLDwnCVf0Km';
        $email = $this->request->getPost('email');
        $pass = $this->request->getPost('password');

        if ($email == 'lukasz@cerebrogaming.com' && $this->security->checkHash($pass, $hash)) {
            $this->session->set('is_logged', true);

            return $this->dispatcher->forward([
                'controller' => 'index',
                'action' => 'index'
            ]);
        }

        return $this->dispatcher->forward([
            'controller' => 'auth',
            'action' => 'index'
        ]);
    }
}
