<?php

namespace Cerebro\Backoffice\Controllers;

use Cerebro\Api\Models\Games;
use Cerebro\Api\Models\TriggersActions;
use Cerebro\Api\Models\TriggersEvents;
use Cerebro\Api\Models\TriggersEventsConditions;
use Cerebro\Api\Models\Triggers;
use Cerebro\Api\Models\TriggersVariables;
use Lib\Helpers\Sort;
use Phalcon\Http\ResponseInterface;

final class TriggersController extends ControllerBase
{
    /*
     * list of triggers
     */
    public function indexAction()
    {
        $triggers = Triggers::find();
        $this->view->setParamToView('list', $triggers->toArray());
    }

    /*
     * edit trigger
     */
    public function editAction(int $triggerId)
    {
        $trigger = Triggers::findFirst($triggerId);
        if (!$trigger) {
            return $this->notFound();
        }

        // all actions/events/conditions
        $actions = TriggersActions::find()->toArray();
        $events = TriggersEvents::find()->toArray();
        $conditions = TriggersEventsConditions::find()->toArray();
        $variables = TriggersVariables::find();
        $games = Games::find();

        Sort::reindexByKey($actions, 'id');
        Sort::reindexByKey($events, 'id');
        Sort::reindexByKey($conditions, 'id');
        Sort::reindexByKey($variables, 'id');

        $triggersEventsConditions = TriggersEventsConditions::getByEventId($trigger->eventId);

        $this->view->setParamToView('variables', $variables);
        $this->view->setParamToView('games', $games);
        $this->view->setParamToView('actions', $actions);
        $this->view->setParamToView('events', $events);
        $this->view->setParamToView('conditions', $conditions);
        $this->view->setParamToView('triggerConditions', $triggersEventsConditions);
        $this->view->setParamToView('trigger', $trigger);
        $this->view->setParamToView('data', json_decode($trigger->dataJson));

        //print_r(json_decode($trigger->dataJson));exit;
    }

    /*
     * add trigger sub page
     */
    public function addAction()
    {
        // all actions/events/conditions
        $actions = TriggersActions::find()->toArray();
        $events = TriggersEvents::find()->toArray();
        $conditions = TriggersEventsConditions::find()->toArray();
        $variables = TriggersVariables::find();
        $games = Games::find();

        Sort::reindexByKey($actions, 'id');
        Sort::reindexByKey($events, 'id');
        Sort::reindexByKey($conditions, 'id');
        Sort::reindexByKey($variables, 'id');

        $this->view->setParamToView('actions', $actions);
        $this->view->setParamToView('events', $events);
        $this->view->setParamToView('conditions', $conditions);
        $this->view->setParamToView('games', $games);
        $this->view->setParamToView('variables', $variables);
    }

    /*
     * Variables for actions
     */
    public function variablesAction()
    {
        $variables = TriggersVariables::find();
        Sort::reindexByKey($variables, 'id');

        $this->view->setParamToView('variables', $variables);
    }

    /*
     * lists actions
     */
    public function listAction(): ResponseInterface
    {
        $triggers = Triggers::find();
        $this->apiResponse->setData($triggers->toArray());

        return $this->respond();
    }


    /*
     * saves trigger
     */
    public function saveAction(): ResponseInterface
    {
        $data = $this->request->getPost();
        if (empty($data['event']['id'])) {
            return $this->incompleteData();
        }

        $params = $this->dispatcher->getParams();
        $trigger = isset($params[0]) ?
            Triggers::findFirst($params[0]) :
            new Triggers();

        $eventId = $data['event']['id'];
        $trigger->eventId = $eventId;
        $trigger->name = $data['name'];
        $trigger->dataJson = \json_encode($data);
        $trigger->save();

        $this->apiResponse->setData(['id' => $trigger->id]);

        return $this->respond();
    }

    /*
     * Returns specific trigger
     */
    public function getAction(int $triggerId): ResponseInterface
    {
        $trigger = Triggers::findFirst($triggerId);
        if (!$trigger) {
            return $this->notFound();
        }

        $this->apiResponse->setData(json_decode($trigger->dataJson));

        return $this->respond();
    }
}
