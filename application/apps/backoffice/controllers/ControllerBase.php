<?php

namespace Cerebro\Backoffice\Controllers;

use Lib\Api\Error;
use Lib\Api\Response;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    /**
     * @var Response
     */
    public $apiResponse;

    /*
     * initialize controller
     */
    public function initialize()
    {
        $this->apiResponse = new Response();

        /*
         * redirect to no access if not logged in
         */
        if (!$this->isAuth()) {
            $controller = $this->router->getControllerName();

            if (!in_array($controller, ['errors', 'auth'])) {
                return $this->response->redirect('/backoffice/login');
            }
        }
    }

    /*
     * This method returns proper json object for API calls
     *
     */
    public function sendApiResponse(): ResponseInterface
    {
        $flag = isProd() ? null : JSON_PRETTY_PRINT;
        if (isProd()) {
            /*
             * this should be on production only
             */
            $this->response->setContentType('application/vnd.api+json', 'UTF-8');
        } else {
            /*
             * this can be displayed in browser so it's only needed for DEV mode
             */
            $this->response->setContentType('application/json', 'UTF-8');
        }

        if ($this->apiResponse->hasErrors()) {
            $this->response->setStatusCode(400, 'Bad request');
        }

        $this->response->setContent(json_encode($this->apiResponse, $flag));
        $this->response->setJsonContent($this->apiResponse);
        return $this->response;
    }

    /*
     * check if call is authenticated
     */
    public function isAuth()
    {
        /**
         * @var \Phalcon\Session\AdapterInterface $session
         */
        $session = $this->di->get('session');
        if ($session->get('is_logged') == true) {
            return true;
        }

        return false;
    }

    /*
     * respond with API object
     */
    public function respond()
    {
        return $this->response->setJsonContent($this->apiResponse);
    }

    /*
     * no access
     */
    public function noAccess(): bool
    {
        $this->response->redirect('/backoffice/errors/forbidden');

        return true;
    }

    /*
     * incomplete data response
     */
    public function incompleteData(): ResponseInterface
    {
        $error = new Error();
        $error->setStatus($error::ERROR_INCOMPLETE_DATA);
        $this->apiResponse->setError($error);

        return $this->respond();
    }

    /*
     * not found response
     */
    public function notFound(): ResponseInterface
    {
        $error = new Error();
        $error->setStatus($error::ERROR_NOT_FOUND);
        $this->apiResponse->setError($error);

        return $this->respond();
    }

    /*
     * method not allowed response
     */
    public function methodNotAllowed(): \Phalcon\Http\Response
    {
        return $this->response->redirect('/backoffice/errors/methodNotAllowed');
    }

    /*
     * error response
     */
    public function error(\Exception $e): bool
    {
        $this->dispatcher->forward(['controller' => 'errors', 'action' => 'exception', 'params' => $e]);
        return true;
    }
}
