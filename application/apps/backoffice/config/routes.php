<?php
/**
 * @var \Phalcon\Mvc\Router $router
 */
$bo = new \Phalcon\Mvc\Router\Group([
    'module' => 'Backoffice',
    'namespace' => '\\Cerebro\\Backoffice\\Controllers',
]);
$bo->setPrefix('/backoffice');

$bo->add('/', ['controller' => 'index', 'action' => 'index'], ['GET']);
$bo->add('/login', ['controller' => 'auth', 'action' => 'index'], ['GET']);
$bo->add('/login', ['controller' => 'auth', 'action' => 'process'], ['POST']);
$bo->add('/logout', ['controller' => 'auth', 'action' => 'logout'], ['GET']);
$bo->add('/triggers', ['controller' => 'triggers', 'action' => 'index'], ['GET']);
$bo->add('/triggers/variables', ['controller' => 'triggers', 'action' => 'variables'], ['GET']);
$bo->add('/triggers/add', ['controller' => 'triggers', 'action' => 'add'], ['GET']);
$bo->add('/triggers/edit/:int', ['controller' => 'triggers', 'action' => 'edit', 'triggerId' => 1], ['GET']);
$bo->add('/triggers/save/:params', ['controller' => 'triggers', 'action' => 'save', 'params' => 1], ['POST']);
$bo->add('/api/triggers/:int', ['controller' => 'triggers', 'action' => 'get', 'triggerId' => 1], ['GET']);

$router->mount($bo);