<?php

namespace Cerebro\Backoffice;

use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\View;

/**
 * Class Module
 *
 * @package Cerebro\Backoffice
 */
final class Module implements ModuleDefinitionInterface
{
    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();

        $loader->registerNamespaces([
            'Cerebro\Backoffice\Controllers' => __DIR__ . '/controllers/',
            'Cerebro\Backoffice\Models' => __DIR__ . '/models/',
            'Cerebro\Api\Models' => __DIR__ . '/../api/models'
        ]);
        $loader->register();
    }

    /**
     * Registers services related to the module
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {
        $config = include __DIR__ . '/config/config.php';
        $di->set('config', $di->get('config')->merge($config));

        $di->set(
            'voltService',
            function ($view, $di) {
                $volt = new View\Engine\Volt($view, $di);

                $volt->setOptions(
                    [
                        'compiledPath' => APP_PATH . '/cache/views/',
                        'compiledExtension' => '.compiled',
                        'compileAlways' => true
                    ]
                );

                return $volt;
            }
        );

        // basic view - won't be used but must be registered
        $di->set('view', function () use ($di) {
            $config = $di->get('config');
            $view = new \Phalcon\Mvc\View();
            $view->setViewsDir($config->application->viewsDir);
            $view->setRenderLevel(View::LEVEL_LAYOUT);
            $view->setLayout('main');

            $view->registerEngines(
                [
                    '.volt' => 'voltService',
                    '.phtml' => 'voltService',
                ]
            );

            return $view;
        }, true);
    }
}
