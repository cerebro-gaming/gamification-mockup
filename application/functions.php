<?php

const ENV_DEV = 'development';
const ENV_STAG = 'staging';
const ENV_PROD = 'production';

/**
 * returns environment (default is production)
 *
 * @return string
 */
function getEnvironment(): string
{
    if (getenv('APPLICATION_ENV') == 'development') {
        return ENV_DEV;
    }

    if (getenv('KNIPSTER_STAG') == 'staging') {
        return ENV_STAG;
    }

    return ENV_PROD;
}

/**
 * @return bool
 */
function isProd(): bool
{
    return (getEnvironment() == ENV_PROD) ? true : false;
}

function isDev(): bool
{
    return (getEnvironment() == ENV_DEV) ? true : false;
}

/**
 * debug function
 *
 * @param mixed $p
 */
function p($p)
{
    echo '<pre>';
    print_r($p);
    echo '</pre>';
}

function pp($p)
{
    p($p);
    exit;
}
